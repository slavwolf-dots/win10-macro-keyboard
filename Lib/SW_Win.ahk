SW_Win_MoveWindow(which_screen, exe_name)
{
    If (which_screen = "primary")
    {
        LocX:=-8
        LocY:=-8
        Width:=2576
        Height:=1426
        WinMove,ahk_exe %exe_name%,,%LocX%,%LocY%,%Width%,%Height%
    }
    Else If (which_screen = "secondary")
    {
        LocX:=2614
        LocY:=-8
        Width:=1154
        Height:=1936
        WinMove,ahk_exe %exe_name%,,%LocX%,%LocY%,%Width%,%Height%
    }
    Return
}