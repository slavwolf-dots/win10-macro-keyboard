#Include Lib\SW_Win.ahk

SetNumlockState, AlwaysOff
SetCapsLockState, AlwaysOff
SetScrollLockState, AlwaysOff
return

; Volume control
F13::
Run, nircmd.exe changeappvolume Discord.exe +0.1
Return

F14::
Run, nircmd.exe changeappvolume Discord.exe -0.1
Return

F15::
WinGet, activeprocess, ProcessName, A
Run, nircmd.exe changeappvolume %activeprocess% +0.1
Return

F16::
WinGet, activeprocess, ProcessName, A
Run, nircmd.exe changeappvolume %activeprocess% -0.1
Return

F17::
Run, nircmd.exe changeappvolume firefox.exe +0.1
Return

F18::
Run, nircmd.exe changeappvolume firefox.exe -0.1
Return

; Process control
^F13::
Process, Exist, Discord.exe
If Not ErrorLevel ; errorlevel will = 0 if process doesn't exist
{
	Run "%localappdata%\Discord\Update.exe" --processStart "Discord.exe"
	WinWait, ahk_exe Discord.exe
	SW_Win_MoveWindow("secondary", "Discord.exe")
	WinActivate,ahk_exe Discord.exe
}
Else
{
	SW_Win_MoveWindow("secondary", "Discord.exe")
	WinActivate,ahk_exe Discord.exe
}
Return

^F15::
Process, Exist, Telegram.exe
If Not ErrorLevel ; errorlevel will = 0 if process doesn't exist
{
	Run, "%appdata%\\Telegram Desktop\\Telegram.exe"
	Sleep 1000
	SW_Win_MoveWindow("secondary", "Telegram.exe")
	WinActivate,ahk_exe Telegram.exe
}
Else
{
	SW_Win_MoveWindow("secondary", "Telegram.exe")
	WinActivate,ahk_exe Telegram.exe
}
Return

^F17::
SW_Win_MoveWindow("secondary", "firefox.exe")
WinActivate,ahk_exe firefox.exe
Return

^F18::
SW_Win_MoveWindow("primary", "firefox.exe")
WinActivate,ahk_exe firefox.exe
Return

; Media control
Pause::
Send, /queue youtube True input{Space}
Return

ScrollLock::
Send {Right 17}
Return

; Free keys
; NumpadIns		0
; NumpadEnd		1
; NumpadDown	2
; NumpadLeft	4
; NumpadClear	5
; NumpadRight 	6
; NumpadHome 	7
; NumpadUp 		8
; NumpadPgUp 	9
; NumpadDel 	.
; NumpadDiv 	/
; NumpadMult 	*
; NumpadAdd 	+
; NumpadSub 	-
; NumpadEnter 	Enter

NumpadPgDn::
SendMessage,0x112,0xF170,2,,Program Manager
Return